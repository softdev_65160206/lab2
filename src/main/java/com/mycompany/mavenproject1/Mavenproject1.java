/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.mavenproject1;

/**
 *
 * @author informatics
 */
import java.util.Scanner;
public class Mavenproject1 {

    public static void main(String[] args) {
        char Board[][] = {{'-','-','-'}, {'-', '-', '-'},{'-', '-', '-'}};
         
        printwelcome();
        while(true){
        printBoard(Board);
        placePiece(Board,"player1");
        if(checkwin(Board).equals("O win")|| checkwin(Board).equals("X win")){
            printBoard(Board);
            System.out.println(checkwin(Board));
            System.out.println("Congratulations!!");
            break;
        }
        printBoard(Board);
        placePiece(Board,"player2");
        if(checkwin(Board).equals("O win")|| checkwin(Board).equals("X win")){
            printBoard(Board);
            System.out.println(checkwin(Board));
            System.out.println("Congratulations!!");
            break;
        }
        }
    }

    static void printwelcome() {
        System.out.println("Welcome to OX");
    }

    static void printBoard(char[][] Board) {
        for (char[] row : Board) {
            for (char col : row) {
                System.out.print(col+" ");
            }
            System.out.println();
        }
    }
    static void placePiece(char[][] Board,String player){
        
        Scanner kb = new Scanner(System.in);
        char symbol = 'X';
        if (player.equals("player1")){
            symbol = 'X';
        }else if(player.equals("player2")){
            symbol = 'O';
        }
        System.out.println(symbol+" turn");
        System.out.print("Please input row,col:");
        String prow = kb.next();
        String pcol = kb.next();
        int prowint = Integer.parseInt(prow);
        int pcolint = Integer.parseInt(pcol);
        for (int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
        if (prowint == i && pcolint == j){
            Board[i][j] = symbol;
            break;
            }
        }
        }
        
    }
    static String checkwin(char[][] Board){
        for(int i = 0; i < 3; i++){
        if(Board[i][0] == Board[i][1]&& Board[i][1]==Board[i][2]&&Board[i][2]!='-'){
            return Board[i][0]+" win";
        }
        else if(Board[0][i] == Board[1][i]&& Board[1][i]==Board[2][i]&&Board[2][i]!='-'){
            return Board[i][0]+" win";
        }
       
    }
        if(Board[0][0] == Board[1][1]&& Board[1][1]==Board[2][2]&&Board[2][2]!='-'){
           return Board[0][0]+" win";     
                } 
        else if(Board[0][2] == Board[1][1]&& Board[1][1]==Board[2][0]&&Board[2][0]!='-'){
           return Board[0][2]+" win";     
                }
        return"";
   
     
}
}

